<?php
/*
Plugin Name: Lightbox Event Tracking
Description: add Google Analytics Event tracking for PrettyPhoto lightbox clicks
Version: 0.1
License: GPL
Author: DMM
Template by: http://web.forret.com/tools/wp-plugin.asp

*/

/* 
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
*/

/*function lightbox_track_clicks () {
	wp_enqueue_script(
		'lightbox-events',
		plugins_url( '/js/lightbox-events.js' , __FILE__ ),
		array( 'jquery' ),
		false,
		true
	);
}

add_action( 'wp_enqueue_scripts', 'lightbox_track_clicks' );
*/
function print_d_s() {
    $cont = '<script>
 jQuery( document ).ready(function(){
	jQuery( "a[rel*=\'light\']" ).click(function () {
					setTimeout(function() { 
		console.log("lightbox");
		jQuery(".pp_arrow_next, .pp_next, .pp_arrow_previous, .pp_previous").off();
		jQuery(".pp_arrow_next, .pp_next").each(function() {
		var act = jQuery(this).html();
		var pic_title = jQuery(".ppt").html();
		var imgsrc = jQuery("#pp_full_res img#fullResImage").attr("src");
		var eventlabel = "Image Name= "+pic_title+" Image File= "+imgsrc+" Next";
		jQuery(this).click(function(event) { 
			event.preventDefault(); 
			setTimeout(function() { 
				console.log(act);
				console.log(imgsrc);
				ga("send", "event", "lightbox", "click-next", eventlabel);
				jQuery.prettyPhoto.changePage("next");
			},200);
		});
	});
		jQuery(".pp_arrow_previous, .pp_previous").each(function() {
		var act = jQuery(this).html();
		var pic_title = jQuery(".ppt").html();
		var imgsrc = jQuery("#pp_full_res img#fullResImage").attr("src");
		var eventlabel = "Image Name= "+pic_title+" Image File= "+imgsrc+" Previous";
		jQuery(this).click(function(event) { 
			event.preventDefault(); 
			setTimeout(function() { 
				console.log(act);
				ga("send", "event", "lightbox", "click-previous", eventlabel);
				jQuery.prettyPhoto.changePage("previous");
			},200);
		});
	});
},1000);

});
});
		
</script>';
echo $cont;
}
add_action('wp_footer', 'print_d_s', 100);
